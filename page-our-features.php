<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <img class="p-0" width="100%" style="aspect-ratio: 17/8;" src="<?php echo get_theme_mod('page_our_features'); ?>" />
    </div>
</div>

<div class="container">
    <div class="row my-4">
        <h1 class="fw-bold text-danger text-center"><u><?php echo $wp_query->post->post_title; ?></u></h1>
        <h2 class="h5 text-center text-dark my-3">More to the Package</h2>

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $wp_query = new WP_Query(array(

            'post_type' => 'our-feature',
            'posts_per_page' => 6,
            'paged' => $paged
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
            <!-- begin loop -->
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                <div class="col-md-4 mb-5">
                    <div class=" mb-3 " style="width: 100%;">
                        <div class="row mt-4">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img class="img-fluid rounded-start event-img" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                            </a>
                            <div class="card-body">
                                <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                    <h5 class="card-title text-center text-dark fw-bold h4"><?php the_title(); ?></h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <?php get_template_part('partials/page', 'links'); ?>
    <?php wp_reset_postdata(); ?>

</div>

<?php get_footer(); ?>