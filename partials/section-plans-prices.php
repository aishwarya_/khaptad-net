<div class="container">
    <div class="row my-5">
        <img class="p-0" width="100%" style="aspect-ratio: 17/8;" src="<?php echo get_theme_mod('page_our_features'); ?>" />
    </div>
</div>

<div class="container">
    <div class="row my-4">
        <a href="plans-prices/">
            <h1 class="fw-bold text-danger text-center my-3"><u>Plans & Prices</u></h1>
        </a>
        <h6 class="text-dark text-center h5 mb-5">More than just internet, its feature packed !</h6>

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'plans-price',
            'posts_per_page' => 3,
            'paged' => $paged,
            // 'order' => 'ASC'
        );

        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="col-md-4">
                    <div class="card border mb-3" style="width: 100%;">
                        <div class="card-header border text-center text-white" style="background-color: red;">
                            <h1><?php the_field('mbps'); ?></h1>
                            <h5><?php the_title(); ?></h5>
                        </div>
                        <div class="card-body text-dark border">
                            <div class="d-flex justify-content-between">
                                <h5><i class="fab fa-safari text-danger"></i> Speed</h5>
                                <h5><?php the_field('mbps'); ?></h5>
                            </div>
                            <div class="d-flex justify-content-between my-2">
                                <h5><i class="fas fa-tv text-danger"></i> TV</h5>
                                <h5><?php the_field('tv'); ?></h5>
                            </div>
                            <div class="d-flex justify-content-between">
                                <h5><i class="fas fa-rocket text-danger"></i> Ultraboost</h5>
                                <a href="ultraboost/">
                                    <button type="button" class="btn btn-success">Learn More</button>
                                </a>
                            </div>
                            <div class="d-flex justify-content-between my-2">
                                <h5><i class="fas fa-clipboard-check text-danger"></i> Secure</h5>
                                <h5><i class="fas fa-check-circle text-success"></i></h5>
                            </div>
                            <div class="d-flex justify-content-between">
                                <h5><i class="far fa-clock text-danger"></i> Service Assurance</h5>
                                <h5><i class="fas fa-check-circle text-success"></i></h5>
                            </div>

                        </div>
                        <div class="text-center text-dark my-2">
                            <h6 class="h5">As low as</h6>
                            <h6 class="text-danger h5"><?php the_field('price'); ?></h6>
                        </div>
                        <div class="card-footer bg-transparent border text-center">
                            <a href="plans-prices/">
                                <button type="button" class="btn btn-danger">See More</button>
                            </a>
                        </div>
                    </div>

                </div>
        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>
</div>