<div class="container">
    <div class="row mt-5">
        <img class="p-0" width="100%" style="aspect-ratio: 17/8;" src="<?php echo get_theme_mod('page_tv'); ?>" />
    </div>
</div>

<div class="container">
    <div class="row my-5">
        <a class="mt-3" href="tv/">
            <h1 class="fw-bold text-danger text-center mb-4"><u>TV</u> <i class="fas fa-tv"></i></h1>
        </a>
        <div class="mb-4 text-center text-dark">
            <h5 class="h4">More than just internet.</h5>
            <h5 class="h4">Now with Khaptad net's TV get full on Entertainment</h5>
        </div>

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'television',
            'posts_per_page' => 6,
            'paged' => $paged,
            // 'order' => 'ASC'
        );

        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="col-md-2 col-sm-3 col-xs-4 text-center">
                    <div class="tv-card">
                        <div class="tv-card-inner">
                            <div class="tv-card-front">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                                <h1 class="h5"><?php the_title(); ?></h1>
                                <div class="mt-4"><i class="fas fa-chevron-down"></i></div>
                            </div>
                            <div class="tv-card-back">
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>

    <div class="row autoplay mb-5">
        <?php
        $wp_query = new WP_Query(array(
            'post_type' => 'tv-image',
            // 'posts_per_page' => 12,
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <div class="col-md-3">
                        <div class="card my-3 p-2">
                            <img class="" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                        </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
