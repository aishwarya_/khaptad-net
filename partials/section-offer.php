<div class="container">
    <div class="row my-5">
        <a href="refer-offer/">
            <h1 class="fw-bold text-danger text-center my-3"><u>Other Exciting Offers</u></h1>
        </a>

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'offers',
            'posts_per_page' => 3,
            'paged' => $paged,
            // 'order' => 'ASC'
        );

        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="col-md-4 mt-4">
                    <div class="card mb-3" style="width: 100%;">
                        <a href="<?php the_permalink(); ?>">
                            <?php if (has_post_thumbnail()) : ?>
                                <img class="img-fluid rounded-start event-img" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                            <?php endif; ?>
                        </a>

                        <div class="card-body">
                            <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                <h5 class="text-center text-danger fw-bold h4"><?php the_title(); ?></h5>
                            </a>
                        </div>
                        <div class="text-center text-dark">
                            <p><?php the_field('offer'); ?></p>
                        </div>
                        <div class="text-center my-4">
                            <a href="refer-offer/">
                                <button type="button" class="btn btn-secondary">View Details</button>
                            </a>
                        </div>
                    </div>

                </div>
        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>
</div>