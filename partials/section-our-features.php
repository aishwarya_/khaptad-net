<div class="container">
    <div class="row my-5">
        <a class="mt-3" href="our-features/">
            <h1 class="fw-bold text-danger text-center"><u>Our Features</u></h1>
        </a>
        <h2 class="h5 text-center text-dark my-3">More to the Package</h2>
        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'our-feature',
            'posts_per_page' => 4,
            'paged' => $paged,
            // 'order' => 'ASC'
        );

        $latest = new WP_Query($args);
        if ($latest->have_posts()) {
            while ($latest->have_posts()) : $latest->the_post();

        ?>
                <div class="col-md-3 mb-5">
                    <div class=" mb-3 cards">
                        <div class="row mt-4">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img class="img-fluid rounded-start event-img" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                            </a>
                            <div class="card-body">
                                <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                                    <h5 class="card-title text-center text-dark fw-bold h4"><?php the_title(); ?></h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            endwhile;
            wp_reset_postdata();
        }
        ?>
    </div>
</div>