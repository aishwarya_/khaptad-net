<?php get_header(); ?>
<div class="container">
    <div class="row mt-4 brand-font">
        <div class="col-md-12">
            <?php
            while (have_posts()) : the_post(); {
            ?>
                    <div class="post-title text-dark">
                        <h1 class="h1 fw-bold"><?php the_title(); ?></h1>
                    </div>
                    <div class="img">
                        <?php if (has_post_thumbnail()) { ?>
                            <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                        <?php  } ?>
                    </div>

                    <div class="single-page img-repo my-4 text-dark" style="font-size: 22px;">
                        <?php the_content(); ?>
                    </div>

                    <div class="comment">
                        <?php echo do_shortcode('[TheChamp-FB-Comments]'); ?>
                    </div>

            <?php
                }

            endwhile;
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>