<?php get_header(); ?>

<div class="container">
    <div class="row my-4">
        <img class="p-0" width="100%" style="aspect-ratio: 17/8;" src="<?php echo get_theme_mod('page_our_features'); ?>" />
    </div>
</div>

<div class="container">
    <div class="row my-4">
        <h1 class="fw-bold text-danger text-center"><u><?php echo $wp_query->post->post_title; ?></u></h1>

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $wp_query = new WP_Query(array(

            'post_type' => 'business-solution',
            'posts_per_page' => 6,
            'paged' => $paged
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
            <!-- begin loop -->
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                <div class="col-md-4 flip-card my-4">
                    <div class="flip-card-inner">
                        <div class="flip-card-front mb-3">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img class="img-fluid rounded-start event-img" width="100%" style="aspect-ratio: 4/3;" src="<?php echo get_the_post_thumbnail_url(null, ''); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                            </a>
                            <!-- <h5 class="text-dark text-center h4 mt-3"><?php the_title(); ?></h5> -->
                        </div>
                        <div class="flip-card-back">
                            <a class="text-white" href="<?php the_permalink(); ?>">
                                <h5 class="card-title text-center fw-bold h4 mt-3"><?php the_title(); ?></h5>
                                <div class="single-page p-3 mb-5">
                                    <p><?php echo custom_excerpt(); ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <?php get_template_part('partials/page', 'links'); ?>

</div>

<?php get_footer(); ?>