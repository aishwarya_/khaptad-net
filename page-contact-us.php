<?php get_header(); ?>
<div class="container">
    <div class="row my-4">
        <h1 class="fw-bold text-danger text-center mb-4"><u><?php echo $wp_query->post->post_title; ?></u></h1>
        <div class="col-md-2">
        
        </div>
        <div class="col-md-8 ">
        <h3 class=" text-danger">Leave Us Feedback</h3>
            <p><?php the_content(); ?></p>
        </div>
        <div class="col-md-2">
        
        </div>
    </div>
</div>

<?php get_footer(); ?>