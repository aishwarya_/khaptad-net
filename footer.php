<div class="container-fluid" style="background-color: #02542c;">
    <!-- Footer -->
    <footer class="text-center text-lg-start text-white">
        <!-- Section: Social media -->
        <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
            <!-- Left -->
            <div class="me-5 d-none d-lg-block">
                <span>Get connected with us on Social Networks :</span>
            </div>
            <!-- Left -->

            <!-- Right -->
            <div>
                <a href="<?php echo get_theme_mod('facebook'); ?>" class="me-4 text-reset">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="<?php echo get_theme_mod('twitter'); ?>" class="me-4 text-reset">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="<?php echo get_theme_mod('google'); ?>" class="me-4 text-reset">
                    <i class="fab fa-google"></i>
                </a>
                <a href="<?php echo get_theme_mod('instragram'); ?>" class="me-4 text-reset">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="<?php echo get_theme_mod('linkedin'); ?>" class="me-4 text-reset">
                    <i class="fab fa-linkedin"></i>
                </a>
                <a href="<?php echo get_theme_mod('youtube'); ?>" class="me-4 text-reset">
                    <i class="fab fa-youtube"></i>
                </a>

            </div>
            <!-- Right -->
        </section>
        <!-- Section: Social media -->

        <!-- Section: Links  -->
        <section class="">
            <div class="container text-center text-md-start mt-3">
                <!-- Grid row -->
                <div class="row mt-3">
                    <!-- Grid column -->
                    <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                        <!-- Content -->
                        <h6 class="text-uppercase fw-bold mb-2">
                            <a href="<?php echo esc_url(home_url()); ?>">
                                <h1 class="fw-bold text-warning h4">KHAPTAD NET</h1>
                            </a>
                        </h6>
                        <p>
                            Here you can use rows and columns to organize your footer content. Lorem ipsum
                            dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-2">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-2">
                            Services
                        </h6>
                        <p>
                            <a href="internet/" class="text-reset">Internet</a>
                        </p>
                        <p>
                            <a href="refer-offer/" class="text-reset">Offer</a>
                        </p>
                        <p>
                            <a href="business-solutions/" class="text-reset">Solutions</a>
                        </p>
                        
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-2">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-2">
                            Useful links
                        </h6>
                        <p>
                            <a href="plans-prices" class="text-reset">Pricing</a>
                        </p>
                        <p>
                            <a href="our-features" class="text-reset">Features</a>
                        </p>
                        <p>
                            <a href="refer-offer" class="text-reset">Offers</a>
                        </p>
                        
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-2">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-2">
                            Contact
                        </h6>
                        <div class="">
                            <div class="home">
                                <i class="fas fa-home mx-1"></i> <?php echo get_theme_mod('home'); ?>
                            </div>
                            <div class="location">
                                <i class="fas fa-map-marker-alt mx-2 my-2"></i> <?php echo get_theme_mod('location'); ?>
                            </div>
                            <div class="phone">
                                <i class="fas fa-phone-alt mx-2"></i> <?php echo get_theme_mod('phone'); ?>
                            </div>
                            <div class="mobile">
                                <i class="fas fa-mobile-alt mx-2 my-2"></i> <?php echo get_theme_mod('mobile'); ?>
                            </div>
                            <div class="email">
                                <i class="far fa-envelope mx-2 mb-2"></i> <?php echo get_theme_mod('email'); ?>
                            </div>
                        </div>

                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
        </section>
        <!-- Section: Links  -->

        <!-- Copyright -->
        <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
            <div class="container text-center h5">
                Copyright &copy; <script>
                    document.write(new Date().getFullYear())
                </script> All Rights Reserved | Developed by <a class="text-monospace text-warning" href="http://mohrain.com"> mohrain</a>
            </div>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
</div>

<?php wp_footer(); ?>

</body>

</html>