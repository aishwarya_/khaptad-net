<?php get_header(); ?>

<div class="container-fluid">
  <div class="row mb-5">
    <div class="col-md-12 p-0">

      <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="4" aria-label="Slide 5"></button>

        </div>
        <div class="carousel-inner">

          <?php
          $args = array(
            'post_type' => 'slider',
            // 'posts_per_page' => 4,
            // 'order' => 'ASC'
          );
          $latest = new WP_Query($args);
          if ($latest->have_posts()) {
            $i = 0;
            while ($latest->have_posts()) : $latest->the_post();
              $i++;
          ?>
              <div class="<?php if ($i == 1) echo 'active' ?> carousel-item">

                <?php if (has_post_thumbnail()) : ?>
                  <div class="text-center">
                    <img class="slider" width="100%" style="aspect-ratio: 9/4;" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php the_title(); ?>">
                  </div>
                  <div class="carousel-caption">
                    <h4 class="h1"><?php the_title(); ?></h4>
                  </div>
                <?php endif; ?>

              </div>

          <?php
            endwhile;
            wp_reset_postdata();
          }
          ?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  </div>
</div>

<!-- including partials folder  here -->

<?php get_template_part('partials/section', 'our-features'); ?>

<?php get_template_part('partials/section', 'tv'); ?>

<?php get_template_part('partials/section', 'plans-prices'); ?>

<?php get_template_part('partials/section', 'offer'); ?>

<?php get_footer(); ?>
