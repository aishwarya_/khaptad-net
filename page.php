<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <img class="p-0" width="100%" style="aspect-ratio: 17/8;" src="<?php echo get_theme_mod('page_image'); ?>" />
    </div>
</div>

<div class="container">
    <div class="row my-4">
        <div class="col-md-12">
            <h1 class="fw-bold text-danger text-center"><u><?php echo $wp_query->post->post_title; ?></u></h1>
            <p><?php the_content(); ?></p>
        </div>
    </div>
</div>

<?php get_footer(); ?>