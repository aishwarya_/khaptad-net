<?php get_header(); ?>

<div class="container-fluid">
    <div class="row">
        <img class="p-0" width="100%" style="aspect-ratio: 17/8;" src="<?php echo get_theme_mod('page_our_features'); ?>" />
    </div>
</div>

<div class="container">
    <div class="row my-4">
        <h1 class="fw-bold text-danger text-center my-3"><u><?php echo $wp_query->post->post_title; ?></u></h1>
        <h6 class="text-dark text-center h5 mb-5">More than just internet, its feature packed !</h6>

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $wp_query = new WP_Query(array(

            'post_type' => 'plans-price',
            'posts_per_page' => 3,
            'paged' => $paged
        ));
        ?>
        <?php if ($wp_query->have_posts()) : ?>
            <!-- begin loop -->
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <div class="col-md-4">
                    <div class="card border mb-3" style="width: 100%;">
                        <div class="card-header border text-center text-white" style="background-color: red;">
                            <h1><?php the_field('mbps'); ?></h1>
                            <h5><?php the_title(); ?></h5>
                        </div>
                        <div class="card-body text-dark border">
                            <div class="d-flex justify-content-between">
                                <h5><i class="fab fa-safari text-danger"></i> Speed</h5>
                                <h5><?php the_field('mbps'); ?></h5>
                            </div>
                            <div class="d-flex justify-content-between my-2">
                                <h5><i class="fas fa-tv text-danger"></i> TV</h5>
                                <h5><?php the_field('tv'); ?></h5>
                            </div>

                            <div class="d-flex justify-content-between my-2">
                                <h5><i class="fas fa-clipboard-check text-danger"></i> Secure</h5>
                                <h5><i class="fas fa-check-circle text-success"></i></h5>
                            </div>
                            <div class="d-flex justify-content-between">
                                <h5><i class="far fa-clock text-danger"></i> Service Assurance</h5>
                                <h5><i class="fas fa-check-circle text-success"></i></h5>
                            </div>

                        </div>
                        <div class="text-center text-dark my-2">
                            <h6 class="h5">As low as</h6>
                            <h6 class="text-danger h5"><?php the_field('price'); ?></h6>
                        </div>
                        <div class="card-footer bg-transparent border text-center">
                            <a href="plans-prices">
                                <button type="button" class="btn btn-danger">See More</button>
                            </a>
                        </div>
                    </div>

                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <?php get_template_part('partials/page', 'links'); ?>


    <div class="row my-5">
        <h1 class="h5 text-dark text-center">We would love to get in touch with you. Leave us your details and our Sales team will contact you within 24 hours.</h1>
        <div class="col-md-4">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label text-dark">Full name</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="name">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label text-dark">Address</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="address">
            </div>
        </div>
        <div class="col-md-4">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label text-dark">Email address</label>
                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label text-dark">City</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="city">
            </div>
        </div>
        <div class="col-md-4">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label text-dark">Mobile No.</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="phone no.">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label text-dark">District</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="district">
            </div>
        </div>

        <div class="button">
            <button type="button" class="btn btn-success">Send</button>
        </div>
    </div>
</div>

<?php get_footer(); ?>