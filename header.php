<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?> |
        <?php is_front_page() ? bloginfo('description') : wp_title(); ?>
    </title>

    <?php wp_head(); ?>
</head>

<body>

    <div class="container-fluid sticky-top" style="background-color: #02542c;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <a href="<?php echo esc_url(home_url()); ?>">
                        <h1 class="fw-bold text-warning mt-2">KHAPTAD NET</h1>
                    </a>
                </div>
                <div class="col-md-9">
                    <nav class=" container-fluid navbar-expand-lg brand-font p-2">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#primarymenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                            <i class="fas fa-bars text-white"></i>
                        </button>
                        <?php
                        wp_nav_menu(array(
                            'theme_location'  => 'primary',
                            'depth'           => 2,
                            'container'       => 'div',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id'    => 'primarymenu',
                            'menu_class'      => 'navbar-nav  justify-content-between w-100 primary-menu',
                            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                            'walker'          => new WP_Bootstrap_Navwalker(),
                        ));
                        ?>
                    </nav>

                </div>
            </div>
        </div>
    </div>