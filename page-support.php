<?php get_header(); ?>
<div class="container">
    <div class="row my-3">
        <h1 class="text-danger text-center mb-2"><u>Welcome to Khaptad Net</u></h1>
        <h5 class="text-center text-dark">Let us help you answer your queries</h5>
    </div>
    <div class="row my-4" style="background-color: #f0f0f0;">
        <h5 class="text-center text-danger mt-3 h4">Prefer to get in Touch ?</h5>
        <h5 class="text-center text-dark mb-4">Reach us using the contact method of your choice :</h5>
        <div class="phone text-success h5">
            <i class="fas fa-phone-alt mx-2"></i> <?php echo get_theme_mod('phone'); ?> | <span>Toll Free No.(NTC) : 1660018846767</span>
        </div>
        <div class="inbox h5 text-success">
        <i class="far fa-comment-dots mx-2 my-2"></i><span>Type KHAPTADNET SUPPORT and SMS to 12005</span>
        </div>
        <div class="mobile text-success h5">
            <i class="fas fa-mobile-alt mx-2 "></i> <?php echo get_theme_mod('mobile'); ?>
        </div>
        <div class="email text-success h5">
            <i class="far fa-envelope mx-2 my-2"></i> <?php echo get_theme_mod('email'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>