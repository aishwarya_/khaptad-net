<?php
function social_customize_register($wp_customize)
{
    $wp_customize->add_section('social_sites_section', array(
        'title' => __('Image Page', ''),
        'priority' => 101
    ));
    // Settings
    $wp_customize->add_setting('page_image', array(
        'default' => get_theme_file_uri('assets/image/logo.jpg'), // Add Default Image URL 
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_setting('page_our_features', array(
        'default' => get_theme_file_uri('assets/image/logo.jpg'), // Add Default Image URL 
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_setting('page_tv', array(
        'default' => get_theme_file_uri('assets/image/logo.jpg'), // Add Default Image URL 
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_setting('instagram_link', array(
        'default' => '',
    ));
    $wp_customize->add_setting('tiktok_link', array(
        'default' => '',
    ));

    // control
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'page_image_control', array(
        'label' => 'Page Image',
        'priority' => 20,
        'section' => 'social_sites_section',
        'settings' => 'page_image',
        'button_labels' => array( // All These labels are optional
            'select' => 'Select Logo',
            'remove' => 'Remove Logo',
            'change' => 'Change Logo',
        )
    )));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'page_our_features_control', array(
        'label' => 'Our Feature Page Image',
        'priority' => 20,
        'section' => 'social_sites_section',
        'settings' => 'page_our_features',
        'button_labels' => array( // All These labels are optional
            'select' => 'Select Logo',
            'remove' => 'Remove Logo',
            'change' => 'Change Logo',
        )
    )));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'page_tv_control', array(
        'label' => 'TV Page Image',
        'priority' => 20,
        'section' => 'social_sites_section',
        'settings' => 'page_tv',
        'button_labels' => array( // All These labels are optional
            'select' => 'Select Logo',
            'remove' => 'Remove Logo',
            'change' => 'Change Logo',
        )
    )));
    $wp_customize->add_control('tiktok_link_control', array(
        'label' => __('Tiktok Link', 'social'),
        'type'  => 'url',
        'section' => 'social_sites_section',
        'settings' => 'tiktok_link',
    ));
}
add_action('customize_register', 'social_customize_register');


