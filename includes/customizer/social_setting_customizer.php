<?php
function page_customize_register($wp_customize)
{
    $wp_customize->add_section('page_sites_section', array(
        'title' => __('Social Network', ''),
        'priority' => 100,
    ));
    // Settings
    $wp_customize->add_setting('facebook', array(
        'default' => '',
    ));
    $wp_customize->add_setting('twitter', array(
        'default' => '',
    ));
    $wp_customize->add_setting('google', array(
        'default' => '',
    ));
    $wp_customize->add_setting('instragram', array(
        'default' => '',
    ));
    $wp_customize->add_setting('linkedin', array(
        'default' => '',
    ));

    
    $wp_customize->add_setting('home', array(
        'default' => '',
    ));
    $wp_customize->add_setting('location', array(
        'default' => '',
    ));
    $wp_customize->add_setting('phone', array(
        'default' => '',
    ));
    $wp_customize->add_setting('email', array(
        'default' => '',
    ));
    $wp_customize->add_setting('mobile', array(
        'default' => '',
    ));

    // Controls
    $wp_customize->add_control('facebook_control', array(
        'label' => __('Facebook Link', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'facebook',
    ));
    $wp_customize->add_control('twitter_control', array(
        'label' => __('Twitter Link', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'twitter',
    ));
    $wp_customize->add_control('google_control', array(
        'label' => __('Google Link', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'google',
    ));
    $wp_customize->add_control('instragram_control', array(
        'label' => __('Instragram Link', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'instragram',
    ));
    $wp_customize->add_control('linkedin_control', array(
        'label' => __('Linkedin Link', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'linkedin',
    ));
    $wp_customize->add_control('youtube_control', array(
        'label' => __('Youtube Link', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'youtube',
    ));

    $wp_customize->add_control('home_control', array(
        'label' => __('Home ', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'home',
    ));
    $wp_customize->add_control('location_control', array(
        'label' => __('Location ', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'location',
    ));
    $wp_customize->add_control('phone_control', array(
        'label' => __('Phone ', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'phone',
    ));
    $wp_customize->add_control('mobile_control', array(
        'label' => __('Mobile', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'mobile',
    ));
    $wp_customize->add_control('email_control', array(
        'label' => __('Email', 'social'),
        'section' => 'page_sites_section',
        'settings' => 'email',
    ));
}
add_action('customize_register', 'page_customize_register');
